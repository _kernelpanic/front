/* ==========Добавить класс активной карточке подписки============*/
// const packageCard = document.querySelectorAll('.package-card');
// for(let i=0; i<packageCard.length; i++ ){
// 	const itemInput = packageCard[i].querySelector('input');
// 	packageCard[i].addEventListener('click', ()=>{
// 		for(let j=0; j<packageCard.length; j++){
// 			if(i==j){
// 				packageCard[i].classList.add('package-card--active');
// 				 itemInput.checked = true;
// 			}
// 			else{
// 				packageCard[j].classList.remove('package-card--active');
// 			}
// 		}
// 	})
// }
function setCookie(c_name,value,exdays){
  var exdate=new Date();
  exdate.setDate(exdate.getDate() + exdays);
  var c_value = escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString()) + "; path=/";
  document.cookie=c_name + "=" + c_value;
}

function getMyCookie(name) {
  var c = document.cookie.match("(^|;) ?" + name + "=([^;]*)(;|$)");
      if (c) return c[2];
   else return "";
}



quantitySecondInCookie()
function quantitySecondInCookie() {
  const date = new Date()
  const quantityDays = 2
  const quanityMilliseconds = quantityDays * 24 * 60 * 60 * 1000
  const totalMilliseconds = date.getTime()
  let timeEnd

  if (getMyCookie("total_milliseconds") == '') {
    timeEnd = (totalMilliseconds + quanityMilliseconds)
    setCookie('total_milliseconds', timeEnd, 2)
  }
}

setTime()
function setTime() {
  const timerInterval = setInterval(() => {
    const date = new Date()
    const timer = parseInt(getMyCookie("total_milliseconds"))
    let remainDate = new Date(timer - date.getTime())
    remainDate = Math.trunc(remainDate.getTime() / 1000)
    
    if (remainDate == 0) {
      clearInterval(timerInterval)
    } else {
      const second = Math.trunc(remainDate%60)
      const minutes = Math.trunc(remainDate/60%60)
      const hours = Math.trunc(remainDate/60/60%60%24)
      const days = Math.trunc(remainDate/60/60/24%24)
  
      const secondElem = document.querySelector('#seconds')
      const minutesElem = document.querySelector('#minutes')
      const hoursElem = document.querySelector('#hours')
      const daysElem = document.querySelector('#days')
  
      secondElem.innerText = addZero(second)
      minutesElem.innerText = addZero(minutes)
      hoursElem.innerText = addZero(hours)
      daysElem.innerText = addZero(days)
    }

    function addZero(value) {
      return value.toString().length == 2 ? value.toString() : '0' + value.toString()
    }
  }, 1000)
}