(function(){
	// Если стартовое время не указано, добавляем его
	if (!localStorage.getItem('time')) localStorage.setItem('time', new Date().getTime());

	var timer = {},
		start = localStorage.getItem('time');

	timer.min = document.querySelector('#minutes');
	timer.sec = document.querySelector('#seconds');

	var addZero = function(value) {
		return value.toString().length == 2 ? value.toString() : '0' + value.toString();
	};

	var count = function(){
		var now = new Date(),
			// Время через 15 минут от нынешнего времени в миллисекундах
			future = new Date(parseInt(start) + 15 * 60 * 1000),
			remain = (future.getTime() - now.getTime()) / 1000;

		if(remain <= 0){
			timer.min.innerHTML = addZero(0);
			timer.sec.innerHTML = addZero(0);

			clearInterval(interval);

			return;
		}else{
			timer.min.innerHTML = addZero(Math.trunc(remain / 60 % 60));
			timer.sec.innerHTML = addZero(Math.trunc(remain % 60));
		}
	};

	var interval = setInterval(count, 1000);
})();