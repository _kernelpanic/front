from sqlalchemy import Column, DateTime, ForeignKey, BigInteger, String, Float, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func
from sqlalchemy.orm import sessionmaker
from sqlalchemy.dialects.postgresql import ARRAY

Base = declarative_base()

class Trx(Base):
    __tablename__ = 'trxes'
    id  = Column(String, primary_key=True, index=True)
    account = Column(String)
    tx_hash = Column(String)
    time_created = Column(DateTime(timezone=True), server_default=func.now())

class UTM(Base):
    __tablename__ = 'utm'
    id  = Column(String, primary_key=True, index=True)
    time_created = Column(DateTime(timezone=True), server_default=func.now())


engine = create_engine('postgresql+psycopg2://bitkongdb:qSuvgEHSi6GFhpOs@db:5432/default_database')

connection = engine.connect()
Session = sessionmaker(bind=engine)
Base.metadata.create_all(engine)
session = Session()