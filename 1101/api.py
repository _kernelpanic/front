import json 
from flask import Flask, render_template, request, redirect, jsonify
from datetime import timedelta, date
# from db import *
from web3 import Web3
from send_token import send_token
from web3.exceptions import TransactionNotFound
from time import sleep
import logging
from db import *
import uuid

logging.basicConfig(filename="logtx.txt",
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)

logging.info("Running TX CHECKER...")

app = Flask(__name__,static_url_path='/static')
app.secret_key = '1E3btRwsoJx2jUcMnATyx7poHhV2tomL8g'

w3_bnb_testnet = Web3(Web3.HTTPProvider('https://bsc-dataseed.binance.org/'))
# w3_bnb_testnet = Web3(Web3.HTTPProvider('https://data-seed-prebsc-1-s1.binance.org:8545'))

@app.route('/get_wallet', methods=['GET', 'POST'])
def get_wallet():
    return "0x04E22aC507a1ef022e01991943AD68F7C0bfD025"

@app.route('/check_tx', methods=['GET'])
def check_tx():
    logging.info("Running check_tx...")
    for _ in range(60):
        
        try:
            tx_status = w3_bnb_testnet.eth.get_transaction_receipt(request.args.get('tx'))
            print(tx_status)
            logging.info(tx_status)
            tx_json = Web3.toJSON(tx_status)

            logging.info(tx_json)

            amount__ = int( request.args.get("amount", 16250000000000000000000) )

            ret = send_token(request.args.get('tx'), request.args.get("to_addr"), amount__)

            logging.info(ret)

            return jsonify(tx_json)
        except TransactionNotFound:
            logging.info("TransactionNotFound")
            print("TransactionNotFound")
        except Exception as e:
            print(e)
            logging.info(e)
            return "Unknown error"

        sleep(1)
    
    print("ERROR TX AT")


@app.route('/', methods=['GET'])
def index():
    new_user = UTM(id=uuid.uuid4().hex)
    session.add(new_user)
    session.commit()
    return render_template("index.html")


# To run locally
if __name__ == '__main__':
    app.run(host='0.0.0.0', port="8000")