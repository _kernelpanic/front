from web3 import Web3
from db import *
from uuid import uuid4

bep20_abi = """[
	{
		"inputs": [
			{
				"internalType": "uint256",
				"name": "initialSupply",
				"type": "uint256"
			}
		],
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "address",
				"name": "owner",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "spender",
				"type": "address"
			},
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "value",
				"type": "uint256"
			}
		],
		"name": "Approval",
		"type": "event"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "spender",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "amount",
				"type": "uint256"
			}
		],
		"name": "approve",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "spender",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "subtractedValue",
				"type": "uint256"
			}
		],
		"name": "decreaseAllowance",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "spender",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "addedValue",
				"type": "uint256"
			}
		],
		"name": "increaseAllowance",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "recipient",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "amount",
				"type": "uint256"
			}
		],
		"name": "transfer",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"anonymous": false,
		"inputs": [
			{
				"indexed": true,
				"internalType": "address",
				"name": "from",
				"type": "address"
			},
			{
				"indexed": true,
				"internalType": "address",
				"name": "to",
				"type": "address"
			},
			{
				"indexed": false,
				"internalType": "uint256",
				"name": "value",
				"type": "uint256"
			}
		],
		"name": "Transfer",
		"type": "event"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "sender",
				"type": "address"
			},
			{
				"internalType": "address",
				"name": "recipient",
				"type": "address"
			},
			{
				"internalType": "uint256",
				"name": "amount",
				"type": "uint256"
			}
		],
		"name": "transferFrom",
		"outputs": [
			{
				"internalType": "bool",
				"name": "",
				"type": "bool"
			}
		],
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "owner",
				"type": "address"
			},
			{
				"internalType": "address",
				"name": "spender",
				"type": "address"
			}
		],
		"name": "allowance",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [
			{
				"internalType": "address",
				"name": "account",
				"type": "address"
			}
		],
		"name": "balanceOf",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "decimals",
		"outputs": [
			{
				"internalType": "uint8",
				"name": "",
				"type": "uint8"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "name",
		"outputs": [
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "symbol",
		"outputs": [
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"stateMutability": "view",
		"type": "function"
	},
	{
		"inputs": [],
		"name": "totalSupply",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"stateMutability": "view",
		"type": "function"
	}
]"""

w3_bnb_testnet = Web3(Web3.HTTPProvider('https://bsc-dataseed.binance.org/'))
# w3_bnb_testnet = Web3(Web3.HTTPProvider('https://data-seed-prebsc-1-s1.binance.org:8545'))



PRIVATE_KEY = '94084a896997137e7c1c64e1b3290988e77edf579128c1019b82059a07b04c41'


def send_token(hash_, receiver_address, amount=16250000000000000000000):

    if session.query(Trx).filter(Trx.tx_hash == hash_).first():
        print("\n\nERROR DUPLICATE WITH ", hash_, receiver_address, amount)
        return "ERROR"

    try:
        new_rec = Trx(id = uuid4().hex,
                      account = receiver_address,
                      tx_hash = hash_)

        session.add(new_rec)
        session.commit()
    except Exception as e:
        print(e)

    print(session.query(Trx).filter(Trx.tx_hash == hash_).first())

    if w3_bnb_testnet.isConnected():

        receiver_address = w3_bnb_testnet.toChecksumAddress(receiver_address )

        my_account = w3_bnb_testnet.eth.account.privateKeyToAccount(PRIVATE_KEY)
        print("my_account ", my_account.address) 
        contract_address = "0xa1E0E7190839A9Def6d3860d338e0E0b154B2202"

        contract = w3_bnb_testnet.eth.contract(contract_address, abi=bep20_abi)
        print(f"My balance: {contract.functions.balanceOf(my_account.address).call()}")
        print(f"Receiver balance: {contract.functions.balanceOf(receiver_address).call()}")

        raw_txn = {
            "from": my_account.address,
            "gasPrice": w3_bnb_testnet.eth.gasPrice,
            "gas": 150000,
            "to": contract_address,
            "value": "0x0",
            "data": contract.encodeABI('transfer', args=(receiver_address, amount)),
            "nonce": w3_bnb_testnet.eth.getTransactionCount(my_account.address)
        }

        signed_txn = w3_bnb_testnet.eth.account.signTransaction(raw_txn, PRIVATE_KEY)
        print("signed_txn ", signed_txn)
        print( w3_bnb_testnet.eth.sendRawTransaction(signed_txn.rawTransaction) )

        return "signed_txn"

    else:
        return "NO NETWORK"







