// Dynamic Adapt v.1
// HTML data-da="where(uniq class name),position(digi),when(breakpoint)"
// e.x. data-da="item,2,992"
// Andrikanych Yevhen 2020
// https://www.youtube.com/c/freelancerlifestyle
(function () {
  var originalPositions = [];
  var daElements = document.querySelectorAll('[data-da]');
  var daElementsArray = [];
  var daMatchMedia = []; //Заполняем массивы

  if (daElements.length > 0) {
	var number = 0;

	for (var index = 0; index < daElements.length; index++) {
	  var daElement = daElements[index];
	  var daMove = daElement.getAttribute('data-da');

	  if (daMove != '') {
		var daArray = daMove.split(',');
		var daPlace = daArray[1] ? daArray[1].trim() : 'last';
		var daBreakpoint = daArray[2] ? daArray[2].trim() : '767';
		var daType = daArray[3] === 'min' ? daArray[3].trim() : 'max';
		var daDestination = document.querySelector('.' + daArray[0].trim());

		if (daArray.length > 0 && daDestination) {
		  daElement.setAttribute('data-da-index', number); //Заполняем массив первоначальных позиций

		  originalPositions[number] = {
			"parent": daElement.parentNode,
			"index": indexInParent(daElement)
		  }; //Заполняем массив элементов 

		  daElementsArray[number] = {
			"element": daElement,
			"destination": document.querySelector('.' + daArray[0].trim()),
			"place": daPlace,
			"breakpoint": daBreakpoint,
			"type": daType
		  };
		  number++;
		}
	  }
	}

	dynamicAdaptSort(daElementsArray); //Создаем события в точке брейкпоинта

	for (var _index = 0; _index < daElementsArray.length; _index++) {
	  var el = daElementsArray[_index];
	  var _daBreakpoint = el.breakpoint;
	  var _daType = el.type;
	  daMatchMedia.push(window.matchMedia("(" + _daType + "-width: " + _daBreakpoint + "px)"));

	  daMatchMedia[_index].addListener(dynamicAdapt);
	}
  } //Основная функция


  function dynamicAdapt(e) {
	for (var _index2 = 0; _index2 < daElementsArray.length; _index2++) {
	  var _el = daElementsArray[_index2];
	  var _daElement = _el.element;
	  var _daDestination = _el.destination;
	  var _daPlace = _el.place;
	  var _daBreakpoint2 = _el.breakpoint;
	  var daClassname = "_dynamic_adapt_" + _daBreakpoint2;

	  if (daMatchMedia[_index2].matches) {
		//Перебрасываем элементы
		if (!_daElement.classList.contains(daClassname)) {
		  var actualIndex = indexOfElements(_daDestination)[_daPlace];

		  if (_daPlace === 'first') {
			actualIndex = indexOfElements(_daDestination)[0];
		  } else if (_daPlace === 'last') {
			actualIndex = indexOfElements(_daDestination)[indexOfElements(_daDestination).length];
		  }

		  _daDestination.insertBefore(_daElement, _daDestination.children[actualIndex]);

		  _daElement.classList.add(daClassname);
		}
	  } else {
		//Возвращаем на место
		if (_daElement.classList.contains(daClassname)) {
		  dynamicAdaptBack(_daElement);

		  _daElement.classList.remove(daClassname);
		}
	  }
	}

	customAdapt();
  } //Вызов основной функции


  dynamicAdapt(); //Функция возврата на место

  function dynamicAdaptBack(el) {
	var daIndex = el.getAttribute('data-da-index');
	var originalPlace = originalPositions[daIndex];
	var parentPlace = originalPlace['parent'];
	var indexPlace = originalPlace['index'];
	var actualIndex = indexOfElements(parentPlace, true)[indexPlace];
	parentPlace.insertBefore(el, parentPlace.children[actualIndex]);
  } //Функция получения индекса внутри родителя


  function indexInParent(el) {
	var children = Array.prototype.slice.call(el.parentNode.children);
	return children.indexOf(el);
  } //Функция получения массива индексов элементов внутри родителя 


  function indexOfElements(parent, back) {
	var children = parent.children;
	var childrenArray = [];

	for (var i = 0; i < children.length; i++) {
	  var childrenElement = children[i];

	  if (back) {
		childrenArray.push(i);
	  } else {
		//Исключая перенесенный элемент
		if (childrenElement.getAttribute('data-da') == null) {
		  childrenArray.push(i);
		}
	  }
	}

	return childrenArray;
  } //Сортировка объекта


  function dynamicAdaptSort(arr) {
	arr.sort(function (a, b) {
	  if (a.breakpoint > b.breakpoint) {
		return -1;
	  } else {
		return 1;
	  }
	});
	arr.sort(function (a, b) {
	  if (a.place > b.place) {
		return 1;
	  } else {
		return -1;
	  }
	});
  } //Дополнительные сценарии адаптации


  function customAdapt() {//const viewport_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  }
})();
/*
let block = document.querySelector('.click');
block.addEventListener("click", function (e) {
	alert('Все ок ;)');
});
*/

/*
//Объявляем переменные
const parent_original = document.querySelector('.content__blocks_city');
const parent = document.querySelector('.content__column_river');
const item = document.querySelector('.content__block_item');
//Слушаем изменение размера экрана
window.addEventListener('resize', move);
//Функция
function move(){
	const viewport_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	if (viewport_width <= 992) {
		if (!item.classList.contains('done')) {
			parent.insertBefore(item, parent.children[2]);
			item.classList.add('done');
		}
	} else {
		if (item.classList.contains('done')) {
			parent_original.insertBefore(item, parent_original.children[2]);
			item.classList.remove('done');
		}
	}
}
//Вызываем функцию
move();
*/

// Lazy Load
!function(t,n){"object"==typeof exports&&"undefined"!=typeof module?module.exports=n():"function"==typeof define&&define.amd?define(n):(t="undefined"!=typeof globalThis?globalThis:t||self).LazyLoad=n()}(this,(function(){"use strict";function t(){return(t=Object.assign||function(t){for(var n=1;n<arguments.length;n++){var e=arguments[n];for(var i in e)Object.prototype.hasOwnProperty.call(e,i)&&(t[i]=e[i])}return t}).apply(this,arguments)}var n="undefined"!=typeof window,e=n&&!("onscroll"in window)||"undefined"!=typeof navigator&&/(gle|ing|ro)bot|crawl|spider/i.test(navigator.userAgent),i=n&&"IntersectionObserver"in window,o=n&&"classList"in document.createElement("p"),r=n&&window.devicePixelRatio>1,a={elements_selector:".lazy",container:e||n?document:null,threshold:300,thresholds:null,data_src:"src",data_srcset:"srcset",data_sizes:"sizes",data_bg:"bg",data_bg_hidpi:"bg-hidpi",data_bg_multi:"bg-multi",data_bg_multi_hidpi:"bg-multi-hidpi",data_poster:"poster",class_applied:"applied",class_loading:"loading",class_loaded:"loaded",class_error:"error",class_entered:"entered",class_exited:"exited",unobserve_completed:!0,unobserve_entered:!1,cancel_on_exit:!0,callback_enter:null,callback_exit:null,callback_applied:null,callback_loading:null,callback_loaded:null,callback_error:null,callback_finish:null,callback_cancel:null,use_native:!1},c=function(n){return t({},a,n)},s=function(t,n){var e,i="LazyLoad::Initialized",o=new t(n);try{e=new CustomEvent(i,{detail:{instance:o}})}catch(t){(e=document.createEvent("CustomEvent")).initCustomEvent(i,!1,!1,{instance:o})}window.dispatchEvent(e)},l="loading",u="loaded",d="applied",f="error",_="native",g="data-",v="ll-status",b=function(t,n){return t.getAttribute(g+n)},p=function(t){return b(t,v)},h=function(t,n){return function(t,n,e){var i="data-ll-status";null!==e?t.setAttribute(i,e):t.removeAttribute(i)}(t,0,n)},m=function(t){return h(t,null)},E=function(t){return null===p(t)},y=function(t){return p(t)===_},I=[l,u,d,f],A=function(t,n,e,i){t&&(void 0===i?void 0===e?t(n):t(n,e):t(n,e,i))},L=function(t,n){o?t.classList.add(n):t.className+=(t.className?" ":"")+n},w=function(t,n){o?t.classList.remove(n):t.className=t.className.replace(new RegExp("(^|\\s+)"+n+"(\\s+|$)")," ").replace(/^\s+/,"").replace(/\s+$/,"")},k=function(t){return t.llTempImage},O=function(t,n){if(n){var e=n._observer;e&&e.unobserve(t)}},x=function(t,n){t&&(t.loadingCount+=n)},z=function(t,n){t&&(t.toLoadCount=n)},C=function(t){for(var n,e=[],i=0;n=t.children[i];i+=1)"SOURCE"===n.tagName&&e.push(n);return e},N=function(t,n,e){e&&t.setAttribute(n,e)},M=function(t,n){t.removeAttribute(n)},R=function(t){return!!t.llOriginalAttrs},T=function(t){if(!R(t)){var n={};n.src=t.getAttribute("src"),n.srcset=t.getAttribute("srcset"),n.sizes=t.getAttribute("sizes"),t.llOriginalAttrs=n}},G=function(t){if(R(t)){var n=t.llOriginalAttrs;N(t,"src",n.src),N(t,"srcset",n.srcset),N(t,"sizes",n.sizes)}},D=function(t,n){N(t,"sizes",b(t,n.data_sizes)),N(t,"srcset",b(t,n.data_srcset)),N(t,"src",b(t,n.data_src))},V=function(t){M(t,"src"),M(t,"srcset"),M(t,"sizes")},j=function(t,n){var e=t.parentNode;e&&"PICTURE"===e.tagName&&C(e).forEach(n)},F={IMG:function(t,n){j(t,(function(t){T(t),D(t,n)})),T(t),D(t,n)},IFRAME:function(t,n){N(t,"src",b(t,n.data_src))},VIDEO:function(t,n){!function(t,e){C(t).forEach((function(t){N(t,"src",b(t,n.data_src))}))}(t),N(t,"poster",b(t,n.data_poster)),N(t,"src",b(t,n.data_src)),t.load()}},P=function(t,n){var e=F[t.tagName];e&&e(t,n)},S=function(t,n,e){x(e,1),L(t,n.class_loading),h(t,l),A(n.callback_loading,t,e)},U=["IMG","IFRAME","VIDEO"],$=function(t,n){!n||function(t){return t.loadingCount>0}(n)||function(t){return t.toLoadCount>0}(n)||A(t.callback_finish,n)},q=function(t,n,e){t.addEventListener(n,e),t.llEvLisnrs[n]=e},H=function(t,n,e){t.removeEventListener(n,e)},B=function(t){return!!t.llEvLisnrs},J=function(t){if(B(t)){var n=t.llEvLisnrs;for(var e in n){var i=n[e];H(t,e,i)}delete t.llEvLisnrs}},K=function(t,n,e){!function(t){delete t.llTempImage}(t),x(e,-1),function(t){t&&(t.toLoadCount-=1)}(e),w(t,n.class_loading),n.unobserve_completed&&O(t,e)},Q=function(t,n,e){var i=k(t)||t;B(i)||function(t,n,e){B(t)||(t.llEvLisnrs={});var i="VIDEO"===t.tagName?"loadeddata":"load";q(t,i,n),q(t,"error",e)}(i,(function(o){!function(t,n,e,i){var o=y(n);K(n,e,i),L(n,e.class_loaded),h(n,u),A(e.callback_loaded,n,i),o||$(e,i)}(0,t,n,e),J(i)}),(function(o){!function(t,n,e,i){var o=y(n);K(n,e,i),L(n,e.class_error),h(n,f),A(e.callback_error,n,i),o||$(e,i)}(0,t,n,e),J(i)}))},W=function(t,n,e){!function(t){t.llTempImage=document.createElement("IMG")}(t),Q(t,n,e),function(t,n,e){var i=b(t,n.data_bg),o=b(t,n.data_bg_hidpi),a=r&&o?o:i;a&&(t.style.backgroundImage='url("'.concat(a,'")'),k(t).setAttribute("src",a),S(t,n,e))}(t,n,e),function(t,n,e){var i=b(t,n.data_bg_multi),o=b(t,n.data_bg_multi_hidpi),a=r&&o?o:i;a&&(t.style.backgroundImage=a,function(t,n,e){L(t,n.class_applied),h(t,d),n.unobserve_completed&&O(t,n),A(n.callback_applied,t,e)}(t,n,e))}(t,n,e)},X=function(t,n,e){!function(t){return U.indexOf(t.tagName)>-1}(t)?W(t,n,e):function(t,n,e){Q(t,n,e),P(t,n),S(t,n,e)}(t,n,e)},Y=["IMG","IFRAME","VIDEO"],Z=function(t){return t.use_native&&"loading"in HTMLImageElement.prototype},tt=function(t,n,e){t.forEach((function(t){return function(t){return t.isIntersecting||t.intersectionRatio>0}(t)?function(t,n,e,i){var o=function(t){return I.indexOf(p(t))>=0}(t);h(t,"entered"),L(t,e.class_entered),w(t,e.class_exited),function(t,n,e){n.unobserve_entered&&O(t,e)}(t,e,i),A(e.callback_enter,t,n,i),o||X(t,e,i)}(t.target,t,n,e):function(t,n,e,i){E(t)||(L(t,e.class_exited),function(t,n,e,i){e.cancel_on_exit&&function(t){return p(t)===l}(t)&&"IMG"===t.tagName&&(J(t),function(t){j(t,(function(t){V(t)})),V(t)}(t),function(t){j(t,(function(t){G(t)})),G(t)}(t),w(t,e.class_loading),x(i,-1),m(t),A(e.callback_cancel,t,n,i))}(t,n,e,i),A(e.callback_exit,t,n,i))}(t.target,t,n,e)}))},nt=function(t){return Array.prototype.slice.call(t)},et=function(t){return t.container.querySelectorAll(t.elements_selector)},it=function(t){return function(t){return p(t)===f}(t)},ot=function(t,n){return function(t){return nt(t).filter(E)}(t||et(n))},rt=function(t,e){var o=c(t);this._settings=o,this.loadingCount=0,function(t,n){i&&!Z(t)&&(n._observer=new IntersectionObserver((function(e){tt(e,t,n)}),function(t){return{root:t.container===document?null:t.container,rootMargin:t.thresholds||t.threshold+"px"}}(t)))}(o,this),function(t,e){n&&window.addEventListener("online",(function(){!function(t,n){var e;(e=et(t),nt(e).filter(it)).forEach((function(n){w(n,t.class_error),m(n)})),n.update()}(t,e)}))}(o,this),this.update(e)};return rt.prototype={update:function(t){var n,o,r=this._settings,a=ot(t,r);z(this,a.length),!e&&i?Z(r)?function(t,n,e){t.forEach((function(t){-1!==Y.indexOf(t.tagName)&&function(t,n,e){t.setAttribute("loading","lazy"),Q(t,n,e),P(t,n),h(t,_)}(t,n,e)})),z(e,0)}(a,r,this):(o=a,function(t){t.disconnect()}(n=this._observer),function(t,n){n.forEach((function(n){t.observe(n)}))}(n,o)):this.loadAll(a)},destroy:function(){this._observer&&this._observer.disconnect(),et(this._settings).forEach((function(t){delete t.llOriginalAttrs})),delete this._observer,delete this._settings,delete this.loadingCount,delete this.toLoadCount},loadAll:function(t){var n=this,e=this._settings;ot(t,e).forEach((function(t){O(t,n),X(t,e,n)}))}},rt.load=function(t,n){var e=c(n);X(t,e)},rt.resetStatus=function(t){m(t)},n&&function(t,n){if(n)if(n.length)for(var e,i=0;e=n[i];i+=1)s(t,e);else s(t,n)}(rt,window.lazyLoadOptions),rt}));

// scrollToElement
!function(t){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=t();else if("function"==typeof define&&define.amd)define([],t);else{("undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this).scrollToElement=t()}}(function(){return function(){return function t(n,e,i){function o(u,c){if(!e[u]){if(!n[u]){var a="function"==typeof require&&require;if(!c&&a)return a(u,!0);if(r)return r(u,!0);var s=new Error("Cannot find module '"+u+"'");throw s.code="MODULE_NOT_FOUND",s}var f=e[u]={exports:{}};n[u][0].call(f.exports,function(t){return o(n[u][1][t]||t)},f,f.exports,t,n,e,i)}return e[u].exports}for(var r="function"==typeof require&&require,u=0;u<i.length;u++)o(i[u]);return o}}()({1:[function(t,n,e){e.linear=function(t){return t},e.inQuad=function(t){return t*t},e.outQuad=function(t){return t*(2-t)},e.inOutQuad=function(t){return(t*=2)<1?.5*t*t:-.5*(--t*(t-2)-1)},e.inCube=function(t){return t*t*t},e.outCube=function(t){return--t*t*t+1},e.inOutCube=function(t){return(t*=2)<1?.5*t*t*t:.5*((t-=2)*t*t+2)},e.inQuart=function(t){return t*t*t*t},e.outQuart=function(t){return 1- --t*t*t*t},e.inOutQuart=function(t){return(t*=2)<1?.5*t*t*t*t:-.5*((t-=2)*t*t*t-2)},e.inQuint=function(t){return t*t*t*t*t},e.outQuint=function(t){return--t*t*t*t*t+1},e.inOutQuint=function(t){return(t*=2)<1?.5*t*t*t*t*t:.5*((t-=2)*t*t*t*t+2)},e.inSine=function(t){return 1-Math.cos(t*Math.PI/2)},e.outSine=function(t){return Math.sin(t*Math.PI/2)},e.inOutSine=function(t){return.5*(1-Math.cos(Math.PI*t))},e.inExpo=function(t){return 0==t?0:Math.pow(1024,t-1)},e.outExpo=function(t){return 1==t?t:1-Math.pow(2,-10*t)},e.inOutExpo=function(t){return 0==t?0:1==t?1:(t*=2)<1?.5*Math.pow(1024,t-1):.5*(2-Math.pow(2,-10*(t-1)))},e.inCirc=function(t){return 1-Math.sqrt(1-t*t)},e.outCirc=function(t){return Math.sqrt(1- --t*t)},e.inOutCirc=function(t){return(t*=2)<1?-.5*(Math.sqrt(1-t*t)-1):.5*(Math.sqrt(1-(t-=2)*t)+1)},e.inBack=function(t){var n=1.70158;return t*t*((n+1)*t-n)},e.outBack=function(t){var n=1.70158;return--t*t*((n+1)*t+n)+1},e.inOutBack=function(t){var n=2.5949095;return(t*=2)<1?t*t*((n+1)*t-n)*.5:.5*((t-=2)*t*((n+1)*t+n)+2)},e.inBounce=function(t){return 1-e.outBounce(1-t)},e.outBounce=function(t){return t<1/2.75?7.5625*t*t:t<2/2.75?7.5625*(t-=1.5/2.75)*t+.75:t<2.5/2.75?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375},e.inOutBounce=function(t){return t<.5?.5*e.inBounce(2*t):.5*e.outBounce(2*t-1)+.5},e.inElastic=function(t){var n,e=.1;return 0===t?0:1===t?1:(!e||e<1?(e=1,n=.1):n=.4*Math.asin(1/e)/(2*Math.PI),-e*Math.pow(2,10*(t-=1))*Math.sin((t-n)*(2*Math.PI)/.4))},e.outElastic=function(t){var n,e=.1;return 0===t?0:1===t?1:(!e||e<1?(e=1,n=.1):n=.4*Math.asin(1/e)/(2*Math.PI),e*Math.pow(2,-10*t)*Math.sin((t-n)*(2*Math.PI)/.4)+1)},e.inOutElastic=function(t){var n,e=.1;return 0===t?0:1===t?1:(!e||e<1?(e=1,n=.1):n=.4*Math.asin(1/e)/(2*Math.PI),(t*=2)<1?e*Math.pow(2,10*(t-=1))*Math.sin((t-n)*(2*Math.PI)/.4)*-.5:e*Math.pow(2,-10*(t-=1))*Math.sin((t-n)*(2*Math.PI)/.4)*.5+1)},e["in-quad"]=e.inQuad,e["out-quad"]=e.outQuad,e["in-out-quad"]=e.inOutQuad,e["in-cube"]=e.inCube,e["out-cube"]=e.outCube,e["in-out-cube"]=e.inOutCube,e["in-quart"]=e.inQuart,e["out-quart"]=e.outQuart,e["in-out-quart"]=e.inOutQuart,e["in-quint"]=e.inQuint,e["out-quint"]=e.outQuint,e["in-out-quint"]=e.inOutQuint,e["in-sine"]=e.inSine,e["out-sine"]=e.outSine,e["in-out-sine"]=e.inOutSine,e["in-expo"]=e.inExpo,e["out-expo"]=e.outExpo,e["in-out-expo"]=e.inOutExpo,e["in-circ"]=e.inCirc,e["out-circ"]=e.outCirc,e["in-out-circ"]=e.inOutCirc,e["in-back"]=e.inBack,e["out-back"]=e.outBack,e["in-out-back"]=e.inOutBack,e["in-bounce"]=e.inBounce,e["out-bounce"]=e.outBounce,e["in-out-bounce"]=e.inOutBounce,e["in-elastic"]=e.inElastic,e["out-elastic"]=e.outElastic,e["in-out-elastic"]=e.inOutElastic},{}],2:[function(t,n,e){function i(t){if(t)return function(t){for(var n in i.prototype)t[n]=i.prototype[n];return t}(t)}i.prototype.on=i.prototype.addEventListener=function(t,n){return this._callbacks=this._callbacks||{},(this._callbacks["$"+t]=this._callbacks["$"+t]||[]).push(n),this},i.prototype.once=function(t,n){function e(){this.off(t,e),n.apply(this,arguments)}return e.fn=n,this.on(t,e),this},i.prototype.off=i.prototype.removeListener=i.prototype.removeAllListeners=i.prototype.removeEventListener=function(t,n){if(this._callbacks=this._callbacks||{},0==arguments.length)return this._callbacks={},this;var e,i=this._callbacks["$"+t];if(!i)return this;if(1==arguments.length)return delete this._callbacks["$"+t],this;for(var o=0;o<i.length;o++)if((e=i[o])===n||e.fn===n){i.splice(o,1);break}return 0===i.length&&delete this._callbacks["$"+t],this},i.prototype.emit=function(t){this._callbacks=this._callbacks||{};var n=[].slice.call(arguments,1),e=this._callbacks["$"+t];if(e)for(var i=0,o=(e=e.slice(0)).length;i<o;++i)e[i].apply(this,n);return this},i.prototype.listeners=function(t){return this._callbacks=this._callbacks||{},this._callbacks["$"+t]||[]},i.prototype.hasListeners=function(t){return!!this.listeners(t).length},void 0!==n&&(n.exports=i)},{}],3:[function(t,n,e){var i=t("./scroll-to");n.exports=function(t,n){if(n=n||{},"string"==typeof t&&(t=document.querySelector(t)),t)return i(0,function(t,n,e){var i,o=document.body,r=document.documentElement,u=t.getBoundingClientRect(),c=r.clientHeight,a=Math.max(o.scrollHeight,o.offsetHeight,r.clientHeight,r.scrollHeight,r.offsetHeight);n=n||0,i="bottom"===e?u.bottom-c:"middle"===e?u.bottom-c/2-u.height/2:u.top;var s=a-c;return Math.min(i+n+window.pageYOffset,s)}(t,n.offset,n.align),n)}},{"./scroll-to":7}],4:[function(t,n,e){(function(t){(function(){var e,i,o,r,u,c;"undefined"!=typeof performance&&null!==performance&&performance.now?n.exports=function(){return performance.now()}:null!=t&&t.hrtime?(n.exports=function(){return(e()-u)/1e6},i=t.hrtime,r=(e=function(){var t;return 1e9*(t=i())[0]+t[1]})(),c=1e9*t.uptime(),u=r-c):Date.now?(n.exports=function(){return Date.now()-o},o=Date.now()):(n.exports=function(){return(new Date).getTime()-o},o=(new Date).getTime())}).call(this)}).call(this,t("_process"))},{_process:5}],5:[function(t,n,e){var i,o,r=n.exports={};function u(){throw new Error("setTimeout has not been defined")}function c(){throw new Error("clearTimeout has not been defined")}function a(t){if(i===setTimeout)return setTimeout(t,0);if((i===u||!i)&&setTimeout)return i=setTimeout,setTimeout(t,0);try{return i(t,0)}catch(n){try{return i.call(null,t,0)}catch(n){return i.call(this,t,0)}}}!function(){try{i="function"==typeof setTimeout?setTimeout:u}catch(t){i=u}try{o="function"==typeof clearTimeout?clearTimeout:c}catch(t){o=c}}();var s,f=[],l=!1,h=-1;function p(){l&&s&&(l=!1,s.length?f=s.concat(f):h=-1,f.length&&d())}function d(){if(!l){var t=a(p);l=!0;for(var n=f.length;n;){for(s=f,f=[];++h<n;)s&&s[h].run();h=-1,n=f.length}s=null,l=!1,function(t){if(o===clearTimeout)return clearTimeout(t);if((o===c||!o)&&clearTimeout)return o=clearTimeout,clearTimeout(t);try{o(t)}catch(n){try{return o.call(null,t)}catch(n){return o.call(this,t)}}}(t)}}function m(t,n){this.fun=t,this.array=n}function w(){}r.nextTick=function(t){var n=new Array(arguments.length-1);if(arguments.length>1)for(var e=1;e<arguments.length;e++)n[e-1]=arguments[e];f.push(new m(t,n)),1!==f.length||l||a(d)},m.prototype.run=function(){this.fun.apply(null,this.array)},r.title="browser",r.browser=!0,r.env={},r.argv=[],r.version="",r.versions={},r.on=w,r.addListener=w,r.once=w,r.off=w,r.removeListener=w,r.removeAllListeners=w,r.emit=w,r.prependListener=w,r.prependOnceListener=w,r.listeners=function(t){return[]},r.binding=function(t){throw new Error("process.binding is not supported")},r.cwd=function(){return"/"},r.chdir=function(t){throw new Error("process.chdir is not supported")},r.umask=function(){return 0}},{}],6:[function(t,n,e){(function(e){for(var i=t("performance-now"),o="undefined"==typeof window?e:window,r=["moz","webkit"],u="AnimationFrame",c=o["request"+u],a=o["cancel"+u]||o["cancelRequest"+u],s=0;!c&&s<r.length;s++)c=o[r[s]+"Request"+u],a=o[r[s]+"Cancel"+u]||o[r[s]+"CancelRequest"+u];if(!c||!a){var f=0,l=0,h=[];c=function(t){if(0===h.length){var n=i(),e=Math.max(0,1e3/60-(n-f));f=e+n,setTimeout(function(){var t=h.slice(0);h.length=0;for(var n=0;n<t.length;n++)if(!t[n].cancelled)try{t[n].callback(f)}catch(t){setTimeout(function(){throw t},0)}},Math.round(e))}return h.push({handle:++l,callback:t,cancelled:!1}),l},a=function(t){for(var n=0;n<h.length;n++)h[n].handle===t&&(h[n].cancelled=!0)}}n.exports=function(t){return c.call(o,t)},n.exports.cancel=function(){a.apply(o,arguments)},n.exports.polyfill=function(t){t||(t=o),t.requestAnimationFrame=c,t.cancelAnimationFrame=a}}).call(this,"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{"performance-now":4}],7:[function(t,n,e){var i=t("./tween"),o=t("raf");n.exports=function(t,n,e){e=e||{};var r={top:window.pageYOffset||document.documentElement.scrollTop,left:window.pageXOffset||document.documentElement.scrollLeft},u=i(r).ease(e.ease||"out-circ").to({top:n,left:t}).duration(e.duration||1e3);function c(){o(c),u.update()}return u.update(function(t){window.scrollTo(0|t.left,0|t.top)}),u.on("end",function(){c=function(){}}),c(),u}},{"./tween":8,raf:6}],8:[function(t,n,e){var i=t("./ease");function o(t){if(!(this instanceof o))return new o(t);this._from=t,this.ease("linear"),this.duration(500)}t("./emitter")(o.prototype),o.prototype.reset=function(){return this.isArray="[object Array]"===Object.prototype.toString.call(this._from),this._curr=function(t,n){for(var e in n)n.hasOwnProperty(e)&&(t[e]=n[e]);return t}({},this._from),this._done=!1,this._start=Date.now(),this},o.prototype.to=function(t){return this.reset(),this._to=t,this},o.prototype.duration=function(t){return this._duration=t,this},o.prototype.ease=function(t){if(!(t="function"==typeof t?t:i[t]))throw new TypeError("invalid easing function");return this._ease=t,this},o.prototype.stop=function(){return this.stopped=!0,this._done=!0,this.emit("stop"),this.emit("end"),this},o.prototype.step=function(){if(!this._done){var t=this._duration,n=Date.now();if(n-this._start>=t)return this._from=this._to,this._update(this._to),this._done=!0,this.emit("end"),this;var e=this._from,i=this._to,o=this._curr,r=(0,this._ease)((n-this._start)/t);if(this.isArray){for(var u=0;u<e.length;++u)o[u]=e[u]+(i[u]-e[u])*r;return this._update(o),this}for(var c in e)o[c]=e[c]+(i[c]-e[c])*r;return this._update(o),this}},o.prototype.update=function(t){return 0==arguments.length?this.step():(this._update=t,this)},n.exports=o},{"./ease":1,"./emitter":2}]},{},[3])(3)});

// Embla carousel
!function(n,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e():"function"==typeof define&&define.amd?define(e):(n="undefined"!=typeof globalThis?globalThis:n||self).EmblaCarousel=e()}(this,(function(){"use strict";function n(){return(n=Object.assign||function(n){for(var e=1;e<arguments.length;e++){var t=arguments[e];for(var r in t)Object.prototype.hasOwnProperty.call(t,r)&&(n[r]=t[r])}return n}).apply(this,arguments)}function e(n,e){var t={start:function(){return 0},center:function(n){return r(n)/2},end:r};function r(n){return e-n}return{measure:function(r){return"number"==typeof n?e*Number(n):t[n](r)}}}function t(n){return n?n/Math.abs(n):0}function r(n,e){return Math.abs(n-e)}function o(n,e){for(var t=[],r=0;r<n.length;r+=e)t.push(n.slice(r,r+e));return t}function i(n){return Object.keys(n).map(Number)}function a(n){return n[u(n)]}function u(n){return Math.max(0,n.length-1)}function c(n,e){var t=n.classList;e&&t.contains(e)&&t.remove(e)}function s(n,e){var t=n.classList;e&&!t.contains(e)&&t.add(e)}function d(){var n=[];var e={add:function(t,r,o,i){return void 0===i&&(i=!1),t.addEventListener(r,o,i),n.push((function(){return t.removeEventListener(r,o,i)})),e},removeAll:function(){return n=n.filter((function(n){return n()})),e}};return e}function f(n){var e=n;function t(n){return e/=n,o}function r(n){return"number"==typeof n?n:n.get()}var o={add:function(n){return e+=r(n),o},divide:t,get:function(){return e},multiply:function(n){return e*=n,o},normalize:function(){return 0!==e&&t(e),o},set:function(n){return e=r(n),o},subtract:function(n){return e-=r(n),o}};return o}function l(n,e,o,i,a,u,c,s,l,p,g,m,v,h){var x=n.scroll,y=n.cross,b=["INPUT","SELECT","TEXTAREA"],w=f(0),M=f(0),S=f(0),E=d(),A=d(),T={mouse:2.5,touch:3.5},C={mouse:5,touch:7},D=a?5:12,P=!1,B=!1,I=!1,L=!1;function z(n){if(!(L="mousedown"===n.type)||0===n.button){var e,t,a=r(i.get(),c.get())>=2,s=L||!a,d=(e=n.target,t=e.nodeName||"",!(b.indexOf(t)>-1)),f=a||L&&d;P=!0,u.pointerDown(n),S.set(i),i.set(c),p.useBaseMass().useSpeed(80),function(){var n=L?document:o;A.add(n,"touchmove",O).add(n,"touchend",k).add(n,"mousemove",O).add(n,"mouseup",k)}(),w.set(u.readPoint(n,x)),M.set(u.readPoint(n,y)),h.emit("pointerDown"),s&&(I=!1),f&&n.preventDefault()}}function O(n){if(!B&&!L){if(!n.cancelable)return k();var t=u.readPoint(n,x).get(),o=u.readPoint(n,y).get(),a=r(t,w.get()),c=r(o,M.get());if(!(B=a>c)&&!I)return k()}var d=u.pointerMove(n);!I&&d&&(I=!0),s.start(),i.add(e.applyTo(d)),n.preventDefault()}function k(){var n=u.pointerUp()*(a?C:T)[L?"mouse":"touch"],o=function(n){var e=!(g.byDistance(0,!1).index!==m.get())&&Math.abs(n)>4,r=n+c.get();if(e&&!a&&!v.reachedAny(r)){var o=m.clone().add(-1*t(n));return g.byIndex(o.get(),0).distance}return g.byDistance(n,!a).distance}(e.applyTo(n)),s=function(n,e){if(0===n||0===e)return 0;if(Math.abs(n)<=Math.abs(e))return 0;var t=r(Math.abs(n),Math.abs(e));return Math.abs(t/n)}(n,o);r(i.get(),S.get())>=.5&&!L&&(I=!0),B=!1,P=!1,A.removeAll(),p.useSpeed(D+D*s),l.distance(o,!a),L=!1,h.emit("pointerUp")}function N(n){I&&n.preventDefault()}return{addActivationEvents:function(){var n=o;E.add(n,"touchmove",(function(){})).add(n,"touchend",(function(){})).add(n,"touchstart",z).add(n,"mousedown",z).add(n,"touchcancel",k).add(n,"contextmenu",k).add(n,"click",N)},clickAllowed:function(){return!I},pointerDown:function(){return P},removeAllEvents:function(){E.removeAll(),A.removeAll()}}}function p(n,e){var t=Math.abs(n-e);function r(e){return e<n}function o(n){return n>e}function i(n){return r(n)||o(n)}return{constrain:function(t){return i(t)?r(t)?n:e:t},length:t,loop:function(t){return i(t)?r(t)?e:n:t},max:e,min:n,reachedAny:i,reachedMax:o,reachedMin:r,removeOffset:function(n){return t?n-t*Math.ceil((n-e)/t):n}}}function g(n,e,r){var o,i,a=(o=2,i=Math.pow(10,o),function(n){return Math.round(n*i)/i}),u=f(0),c=f(0),s=f(0),d=0,l=e,p=r;function g(n){return l=n,v}function m(n){return p=n,v}var v={direction:function(){return d},seek:function(e){s.set(e).subtract(n);var r,o,i,a=(r=s.get(),(i=0)+(r-(o=0))/(100-o)*(l-i));return d=t(s.get()),s.normalize().multiply(a).subtract(u),function(n){n.divide(p),c.add(n)}(s),v},settle:function(e){var t=e.get()-n.get(),r=!a(t);return r&&n.set(e),r},update:function(){u.add(c),n.add(u),c.multiply(0)},useBaseMass:function(){return m(r)},useBaseSpeed:function(){return g(e)},useMass:m,useSpeed:g};return v}function m(n,e,t){var r=!1;return{constrain:function(o,i){if(function(t){return!r&&(!!n.reachedAny(t.get())&&!!n.reachedAny(e.get()))}(o)){var a=i?.7:.4,u=o.get()-e.get();o.subtract(u*a),!i&&Math.abs(u)<10&&(o.set(n.constrain(o.get())),t.useSpeed(10).useMass(3))}},toggleActive:function(n){r=!n}}}function v(n,e,t,r,o){var i=p(-e+n,t[0]),u=r.map(i.constrain);return{snapsContained:function(){if(e<=n)return[i.max];if("keepSnaps"===o)return u;var t=function(){var n=u[0],e=a(u),t=u.lastIndexOf(n),r=u.indexOf(e)+1;return p(t,r)}(),r=t.min,c=t.max;return u.slice(r,c)}()}}function h(n,e,t,r){var o=p(t.min+e.measure(.1),t.max+e.measure(.1)),i=o.reachedMin,a=o.reachedMax;return{loop:function(e,t){if(function(n){return 1===n?a(r.get()):-1===n&&i(r.get())}(t)){var o=n*(-1*t);e.forEach((function(n){return n.add(o)}))}}}}function x(n){var e=n.max,t=n.length;return{get:function(n){return(n-e)/-t}}}function y(n,e,t,r,i,u){var c,s,d=n.startEdge,f=n.endEdge,l=i.map((function(n){return r[d]-n[d]})).map(t.measure).map((function(n){return-Math.abs(n)})),p=(c=o(l,u).map((function(n){return n[0]})),s=o(i,u).map((function(n){return a(n)[f]-n[0][d]})).map(t.measure).map(Math.abs).map(e.measure),c.map((function(n,e){return n+s[e]})));return{snaps:l,snapsAligned:p}}function b(n,e,t,r,o){var i=r.reachedAny,a=r.removeOffset,u=r.constrain;function c(n,e){return Math.abs(n)<Math.abs(e)?n:e}function s(e,r){var o=e,i=e+t,a=e-t;if(!n)return o;if(!r)return c(c(o,i),a);var u=c(o,1===r?i:a);return Math.abs(u)*r}return{byDistance:function(t,r){var c=o.get()+t,d=function(t){var r=n?a(t):u(t);return{index:e.map((function(n){return n-r})).map((function(n){return s(n,0)})).map((function(n,e){return{diff:n,index:e}})).sort((function(n,e){return Math.abs(n.diff)-Math.abs(e.diff)}))[0].index,distance:r}}(c),f=d.index,l=d.distance,p=!n&&i(c);return!r||p?{index:f,distance:t}:{index:f,distance:t+s(e[f]-l,0)}},byIndex:function(n,t){return{index:n,distance:s(e[n]-o.get(),t)}},shortcut:s}}function w(n,e,t,r,o,a,u){var c,s=i(r),d=i(r).reverse(),f=(c=o[0]-1,g(p(d,c),"end")).concat(function(){var n=e-o[0]-1;return g(p(s,n),"start")}());function l(n,e){return n.reduce((function(n,e){return n-r[e]}),e)}function p(n,e){return n.reduce((function(n,t){return l(n,e)>0?n.concat([t]):n}),[])}function g(n,e){var r="start"===e,o=r?-t:t,i=a.findSlideBounds(o);return n.map((function(n){var e=r?0:-t,o=r?t:0,a=i.filter((function(e){return e.index===n}))[0][r?"end":"start"];return{point:a,getTarget:function(){return u.get()>a?e:o},index:n,location:-1}}))}return{canLoop:function(){return f.every((function(n){var t=n.index;return l(s.filter((function(n){return n!==t})),e)<=0}))},clear:function(e){f.forEach((function(t){var r=t.index;e[r].style[n.startEdge]=""}))},loop:function(e){f.forEach((function(t){var r=t.getTarget,o=t.location,i=t.index,a=r();a!==o&&(e[i].style[n.startEdge]=a+"%",t.location=a)}))},loopPoints:f}}function M(n,e,t){var r=d(),o=r.removeAll,i=0;function a(n){9===n.keyCode&&(i=(new Date).getTime())}function u(o,a){r.add(o,"focus",(function(){if(!((new Date).getTime()-i>10)){n.scrollLeft=0;var r=Math.floor(a/t);e.index(r,0)}}),!0)}return{addActivationEvents:function(n){r.add(document,"keydown",a,!1),n.forEach(u)},removeAllEvents:o}}function S(n,e,t){var r=t.style,o="x"===n.scroll?function(n){return"translate3d("+n+"%,0px,0px)"}:function(n){return"translate3d(0px,"+n+"%,0px)"},i=!1;return{clear:function(){r.transform=""},to:function(n){i||(r.transform=o(e.applyTo(n.get())))},toggleActive:function(n){i=!n}}}function E(n,r,o,c,s){var d,E=c.align,A=c.axis,T=c.direction,C=c.startIndex,D=c.inViewThreshold,P=c.loop,B=c.speed,I=c.dragFree,L=c.slidesToScroll,z=c.containScroll,O=r.getBoundingClientRect(),k=o.map((function(n){return n.getBoundingClientRect()})),N=function(n){var e="rtl"===n?-1:1;return{applyTo:function(n){return n*e}}}(T),F=function(n,e){var t="y"===n?"y":"x";return{scroll:t,cross:"y"===n?"x":"y",startEdge:"y"===t?"top":"rtl"===e?"right":"left",endEdge:"y"===t?"bottom":"rtl"===e?"left":"right",measureSize:function(n){var e=n.width,r=n.height;return"x"===t?e:r}}}(A,T),U=(d=F.measureSize(O),{measure:function(n){return 0===d?0:n/d*100},totalPercent:100}),V=U.totalPercent,H=e(E,V),R=function(n,e,t,r,o){var i=n.measureSize,c=n.startEdge,s=n.endEdge,d=r.map(i);return{slideSizes:d.map(e.measure),slideSizesWithGaps:r.map((function(n,e,r){var i=e===u(r),f=window.getComputedStyle(a(t)),l=parseFloat(f.getPropertyValue("margin-"+s));return i?d[e]+(o?l:0):r[e+1][c]-n[c]})).map(e.measure).map(Math.abs)}}(F,U,o,k,P),j=R.slideSizes,G=R.slideSizesWithGaps,q=y(F,H,U,O,k,L),W=q.snaps,X=q.snapsAligned,J=-1*a(W)+a(G),Y=v(V,J,W,X,z).snapsContained,K=!P&&""!==z?Y:X,Q=function(n,e,t){var r,o;return{limit:(r=e[0],o=a(e),p(t?r-n:o,r))}}(J,K,P).limit,Z=function n(e,r,o){var i=e.min,a=e.max,u=e[r?"loop":"constrain"],c=u(o);function s(){return c}function d(n){return c=u(n),f}var f={add:function n(e){if(0!==e){var r=t(e);return d(s()+r),n(e+-1*r)}return f},clone:function(){return n(e,r,s())},get:s,max:a,min:i,set:d};return f}(p(0,u(K)),P,C),$=Z.clone(),_=i(o),nn=function(n){var e=0;function t(n,t){return function(){n===!!e&&t()}}function r(){e=window.requestAnimationFrame(n)}return{proceed:t(!0,r),start:t(!1,r),stop:t(!0,(function(){window.cancelAnimationFrame(e),e=0}))}}((function(){P||dn.scrollBounds.constrain(rn,dn.dragHandler.pointerDown()),dn.scrollBody.seek(rn).update();var n=dn.scrollBody.settle(rn);n&&!dn.dragHandler.pointerDown()&&(dn.animation.stop(),s.emit("settle")),n||s.emit("scroll"),P&&(dn.scrollLooper.loop(on,dn.scrollBody.direction()),dn.slideLooper.loop(o)),dn.translate.to(tn),dn.animation.proceed()})),en=K[Z.get()],tn=f(en),rn=f(en),on=[tn,rn],an=g(tn,B,1),un=b(P,K,J,Q,rn),cn=function(n,e,t,r,o,i){function a(r){var a=r.distance,u=r.index!==e.get();a&&(n.start(),o.add(a)),u&&(t.set(e.get()),e.set(r.index),i.emit("select"))}return{distance:function(n,e){a(r.byDistance(n,e))},index:function(n,t){var o=e.clone().set(n);a(r.byIndex(o.get(),t))}}}(nn,Z,$,un,rn,s),sn=function(n,e,t,r,o,i){var a=Math.min(Math.max(i,.01),.99),u=(o?[0,e,-e]:[0]).reduce((function(n,e){return n.concat(c(e,a))}),[]);function c(e,o){var i=t.map((function(n){return n*(o||0)}));return r.map((function(r,o){return{start:r-t[o]+i[o]+e,end:r+n-i[o]+e,index:o}}))}return{check:function(n){return u.reduce((function(e,t){var r=t.index,o=t.start,i=t.end;return!(-1!==e.indexOf(r))&&(o<n&&i>n)?e.concat([r]):e}),[])},findSlideBounds:c}}(V,J,j,W,P,D),dn={animation:nn,axis:F,direction:N,dragHandler:l(F,N,n,rn,I,function(n,e){var t=n.scroll,r={x:"clientX",y:"clientY"},o=f(0),i=f(0),a=f(0),u=f(0),c=[],s=(new Date).getTime(),d=!1;function l(n,e){d=!n.touches;var t=r[e],o=d?n[t]:n.touches[0][t];return u.set(o)}return{pointerDown:function(n){var r=l(n,t);return o.set(r),a.set(r),e.measure(o.get())},pointerMove:function(n){var r=l(n,t),o=(new Date).getTime(),u=o-s;return u>=10&&(u>=100&&(c=[]),c.push(r.get()),s=o),i.set(r).subtract(a),a.set(r),e.measure(i.get())},pointerUp:function(){var n=(new Date).getTime()-s,t=a.get(),r=c.slice(-5).map((function(n){return t-n})).sort((function(n,e){return Math.abs(n)<Math.abs(e)?1:-1}))[0];return a.set(n>100||!r?0:r),c=[],e.measure(a.get())},readPoint:l}}(F,U),tn,nn,cn,an,un,Z,Q,s),pxToPercent:U,index:Z,indexPrevious:$,limit:Q,location:tn,options:c,scrollBody:an,scrollBounds:m(Q,tn,an),scrollLooper:h(J,U,Q,tn),scrollProgress:x(Q),scrollSnaps:K,scrollTarget:un,scrollTo:cn,slideFocus:M(n,cn,L),slideLooper:w(F,V,J,G,K,sn,tn),slidesInView:sn,slideIndexes:_,target:rn,translate:S(F,N,r)};return dn}var A={align:"center",axis:"x",containScroll:"",direction:"ltr",dragFree:!1,draggable:!0,draggableClass:"is-draggable",draggingClass:"is-dragging",inViewThreshold:0,loop:!1,selectedClass:"is-selected",slidesToScroll:1,speed:10,startIndex:0};return function(e,t){var r,o,i,a,u,f,l,p=function(){var n={};function e(e){return n[e]||[]}var t={emit:function(n){return e(n).forEach((function(e){return e(n)})),t},off:function(r,o){return n[r]=e(r).filter((function(n){return n!==o})),t},on:function(r,o){return n[r]=e(r).concat([o]),t}};return t}(),g=d(),m=(r=function(){if(y){var n=a.axis.measureSize(e.getBoundingClientRect());M!==n&&B(),p.emit("resize")}},o=500,i=0,function(){window.clearTimeout(i),i=window.setTimeout(r,o)||0}),v=B,h=p.on,x=p.off,y=!1,b=n({},A),w=n({},b),M=0;function S(){if(!e)throw new Error("Missing root node 😢");var n,t,r=e.querySelector("*");if(!r)throw new Error("Missing container node 😢");f=r,l=Array.prototype.slice.call(f.children),n=e,t=window.getComputedStyle(n,":before").content,u={get:function(){try{return JSON.parse(t.slice(1,-1).replace(/\\/g,""))}catch(n){}return{}}}}function T(t){if(S(),b=n({},b,t),w=n({},b,u.get()),a=E(e,f,l,w,p),g.add(window,"resize",m),a.translate.to(a.location),M=a.axis.measureSize(e.getBoundingClientRect()),w.loop){if(!a.slideLooper.canLoop())return P(),T({loop:!1});a.slideLooper.loop(l)}w.draggable&&f.offsetParent&&l.length&&(a.dragHandler.addActivationEvents(),w.draggableClass&&s(e,w.draggableClass),w.draggingClass&&p.on("pointerDown",C).on("pointerUp",C)),l.length&&a.slideFocus.addActivationEvents(l),w.selectedClass&&(D(),p.on("select",D).on("pointerUp",D)),y||(setTimeout((function(){return p.emit("init")}),0),y=!0)}function C(n){var t=w.draggingClass;"pointerDown"===n?s(e,t):c(e,t)}function D(){var n=w.selectedClass,e=I(!0);L(!0).forEach((function(e){return c(l[e],n)})),e.forEach((function(e){return s(l[e],n)}))}function P(){a.dragHandler.removeAllEvents(),a.slideFocus.removeAllEvents(),a.animation.stop(),g.removeAll(),a.translate.clear(),a.slideLooper.clear(l),c(e,w.draggableClass),l.forEach((function(n){return c(n,w.selectedClass)})),p.off("select",D).off("pointerUp",D).off("pointerDown",C).off("pointerUp",C)}function B(e){if(y){var t=n({startIndex:O()},e);P(),T(t),p.emit("reInit")}}function I(n){var e=a[n?"target":"location"].get(),t=w.loop?"removeOffset":"constrain";return a.slidesInView.check(a.limit[t](e))}function L(n){var e=I(n);return a.slideIndexes.filter((function(n){return-1===e.indexOf(n)}))}function z(n,e){a.scrollBody.useBaseMass().useBaseSpeed(),y&&a.scrollTo.index(n,e||0)}function O(){return a.index.get()}return T(t),{canScrollNext:function(){return a.index.clone().add(1).get()!==O()},canScrollPrev:function(){return a.index.clone().add(-1).get()!==O()},clickAllowed:function(){return a.dragHandler.clickAllowed()},containerNode:function(){return f},dangerouslyGetEngine:function(){return a},destroy:function(){y&&(P(),y=!1,p.emit("destroy"))},off:x,on:h,previousScrollSnap:function(){return a.indexPrevious.get()},reInit:v,rootNode:function(){return e},scrollNext:function(){z(a.index.clone().add(1).get(),-1)},scrollPrev:function(){z(a.index.clone().add(-1).get(),1)},scrollProgress:function(){return a.scrollProgress.get(a.location.get())},scrollSnapList:function(){return a.scrollSnaps.map(a.scrollProgress.get)},scrollTo:z,selectedScrollSnap:O,slideNodes:function(){return l},slidesInView:I,slidesNotInView:L}}}));

// Контроль блокировки body для разблокировки только при закрытии всех окон и меню
(function(w){
	var BodyBlockingControl = function(){
		this.body = document.body;
		this.className = 'blocked';
		this.attr = 'data-body-scroll-fix';
		this.bar = window.innerWidth - document.querySelector(".wrapper").offsetWidth;
		this.opened = 0;
	};

	var p = BodyBlockingControl.prototype;

	p.block = function(){
		this.opened++;

		this.body.classList.add(this.className);

		if (!this.body.hasAttribute(this.attr)) {

			// Получаем позицию прокрутки
			var scrollPosition = window.pageYOffset || document.documentElement.scrollTop;

			// Ставим нужные стили
			this.body.setAttribute(this.attr, scrollPosition); // Cтавим атрибут со значением прокрутки
			this.body.style.overflow = 'hidden';
			this.body.style.position = 'fixed';
			this.body.style.top = '-' + scrollPosition + 'px';
			this.body.style.left = '0';
			this.body.style.width = '100%';
			this.body.style.paddingRight = this.bar + 'px';
		}
	};

	p.unblock = function(){
		this.opened--;

		if(this.opened > 0) return false;

		this.body.classList.remove(this.className);
		this.body.style.paddingRight = '';

		if (this.body.hasAttribute(this.attr)) {

			// Получаем позицию прокрутки из атрибута
			var scrollPosition = this.body.getAttribute(this.attr);

			// Удаляем атрибут
			this.body.removeAttribute(this.attr);

			// Удаляем ненужные стили
			this.body.style.overflow = '';
			this.body.style.position = '';
			this.body.style.top = '';
			this.body.style.left = '';
			this.body.style.width = '';
			this.body.style.paddingRight = '';

			// Прокручиваем страницу на полученное из атрибута значение
			window.scroll(0, scrollPosition);

		}
	};

	p.check = function(){
		if(this.body.classList.contains('blocked')) return true;
		return false;
	};

	w.BodyBlockingControl = BodyBlockingControl;
})(window);

// modal
(function(w){
	var Mmodal = function(bb){
		this.m = document.querySelector('.modal');

		if(!this.m) return false;

		this.ir = this.m.querySelector('.modal__inner');
		this.o = this.m.querySelector('.modal__overlay');
		this.c = this.m.querySelectorAll('.modal-close');
		this.ms = this.m.querySelectorAll('.modal-item');
		this.b = document.body;
		this.l = this.m.querySelector('.modal-loader');
		this.out = this.m.querySelector('.modal__outer');
		this.timeout = 300;
		this.bb = bb;

		this.bindHandlers();
		this.addListeners();
		
	};

	var p = Mmodal.prototype;

	p.each = function(items, callback){
		[].forEach.call(items, callback);
	};

	// Высота экрана с учетом нижней подложки в Safari
	p.windowHeight = function(el) {
		var vh = window.innerHeight * 0.01;
		el.style.setProperty('--vh', vh + 'px');
	};

	p.hideItems = function(){
		this.each(this.ms, function (e) {
			e.style.display = 'none';
		});
	};

	p.closeModalHandler = function(event){
		event.preventDefault();
		
		this.close();
	};

	p.resizeWindowHandler = function(){
		this.windowHeight(this.m);
	};

	p.close = function(){
		this.ir.classList.remove('open');
		this.m.classList.remove('open');
		this.o.removeAttribute('style');

		var self = this;

		setTimeout(function () {
			self.hideItems();
			self.m.style.display = 'none';

			self.bb.unblock();

			window.removeEventListener('resize', self.resizeWindowHandler);
		}, this.timeout);
	};

	p.open = function(string){
		var modal = this.m.querySelector(string);

		this.hideItems();
		modal.removeAttribute('style');
		this.m.removeAttribute('style');
		this.bb.block();
		this.windowHeight(this.m);

		window.addEventListener('resize', this.resizeWindowHandler);

		var self = this;

		setTimeout(function () {
			self.ir.classList.add('open');
			self.m.classList.add('open');
			self.o.style.width = 'calc(100% - ' + self.bb.bar + 'px)';
			self.l.style.display = 'none';
		}, 2);
	};

	p.change = function(){
		this.ir.classList.remove('open');
		setTimeout(this.hideItems, this.timeout);
	};

	p.loader = function(){
		this.m.removeAttribute('style');
		this.l.removeAttribute('style');

		if(!this.bb.check()) this.bb.block();

		var self = this;

		setTimeout(function () {
			self.m.classList.add('open');
			self.o.style.width = 'calc(100% - ' + self.bb.bar + 'px)';
		}, 2);
	};

	p.addListeners = function() {
		var self = this;

		this.each(this.c, function (e) {
			e.addEventListener('click', self.closeModalHandler);
		});

		this.o.addEventListener('click', self.closeModalHandler);
	};

	p.bindHandlers = function () {
		this.closeModalHandler = this.closeModalHandler.bind(this);
		this.resizeWindowHandler = this.resizeWindowHandler.bind(this);
	};

	w.Mmodal = Mmodal;
})(window);

// Quiz
(function(w){
	var Quiz =  function(element){
		if(!element) return;

		if (typeof element === 'string') {
			element = document.querySelectorAll(element);
		}

		if (element.length > 1) {
			return Quiz.createMany(element);
		} else if (element.length === 1) {
			element = element[0];
		}

		if(element.length === 0) return;

		this.main = element;
		this.form = this.main.querySelector('.quiz__form');
		this.error = this.main.querySelector('.quiz__error');
		this.next = this.main.querySelector('.quiz__button_next');
		this.send = this.main.querySelector('.quiz__button_send');
		this.prev = this.main.querySelector('.quiz__back');
		this.header = this.main.querySelector('.quiz__header');
		this.footer = this.main.querySelector('.quiz__footer');
		this.counter = this.main.querySelector('.quiz__counter');
		this.progress = this.main.querySelector('.quiz__progress-inner');
		this.steps = this.main.querySelectorAll('.quiz__item');
		this.today = new Date();
		this.stepPersent = 100 / this.steps.length;
		this.currentPersent = 0;
		this.currentIndex = 0;
		this.count = this.steps.length - 1;
		this.completed = [];

		var self = this;

		this.each(this.steps, function(item, index){
			if(index + 1 <= self.count) self.completed.push(null);
		});

		this.addPersent();
		this.updateCounter();
		this.bindHandlers();
		this.addButtonsListeners();
		this.addFormListener();
		this.checkCompleted();

		this.each(this.steps, function(step){
			self.manageStep(step);
		});
	};

	Quiz.createMany = function (nodes) {
		var result = [];
		for (var i = 0; i < nodes.length; i++) {
			result.push(new Quiz(nodes[i]));
		}
		return result;
	};

	var p = Quiz.prototype;

	p.each = function(items, callback){
		[].forEach.call(items, callback);
	};

	p.show = function(item){
		item.style.display = '';
	};

	p.hide = function(item){
		item.style.display = 'none';
	};

	p.hideSteps = function(){
		var self = this;

		this.each(this.steps, function(item){
			self.hide(item);
		});
	};

	p.bindHandlers = function () {
		this.checkButton = this.checkButton.bind(this);
		this.prevStep = this.prevStep.bind(this);
		this.nextStep = this.nextStep.bind(this);
		// this.sendData = this.sendData.bind(this);
		this.checkVariation = this.checkVariation.bind(this);
	};

	p.addPersent = function(){
		if(this.currentPersent >= 100){
			this.currentPersent = 100;
			this.progress.style.width = '100%';
			return;
		}

		this.currentPersent += this.stepPersent;
		this.progress.style.width = this.currentPersent + '%';
	};

	p.removePersent = function(){
		if(this.currentPersent <= 0){
			this.currentPersent = 0;
			this.progress.style.width = '0%';
			return;
		}

		this.currentPersent -= this.stepPersent;
		this.progress.style.width = this.currentPersent + '%';
	};

	p.updateCounter = function(){
		var current = this.currentIndex + 1;

		if (current < this.count) this.counter.innerHTML = 'Вопрос ' + (this.currentIndex + 1) + ' из ' + this.count;
		if (current === this.count) this.counter.innerHTML = 'Почти всё, остался 1 шаг';
	};

	p.nextStep = function(){
		this.hideSteps();

		this.currentIndex++;

		this.show(this.steps[this.currentIndex]);
		this.addPersent();
		this.updateCounter();

		if(this.count === this.currentIndex){
			this.hide(this.header);
			this.hide(this.footer);
		}

		if(this.count - 1 === this.currentIndex){
			this.show(this.send);
			this.hide(this.next);
			this.hide(this.prev);
		}

		if(this.count - 1 > this.currentIndex) this.show(this.prev);

		if(this.count - 1 >= this.currentIndex) this.checkCompleted();
	};

	p.prevStep = function(){
		if(this.currentIndex === 0) return false;

		this.hideSteps();

		this.currentIndex--;

		this.show(this.steps[this.currentIndex]);
		this.removePersent();
		this.updateCounter();

		if(this.count - 1 === this.currentIndex){
			this.show(this.header);
			this.show(this.footer);
		}

		if (this.count - 1 > this.currentIndex){
			this.hide(this.send);
			this.show(this.next);
		}

		if(this.count - 1 > this.currentIndex) this.show(this.prev);
		if(this.currentIndex === 0) this.hide(this.prev);

		this.checkCompleted();
	};

	p.timoutNext = function(){
		setTimeout(this.nextStep, 300);
	};

	p.getStepType = function(step){
		if (step.querySelector('.quiz__answers')) return 'answers';
		if (step.querySelector('.quiz__date')) return 'date';
		if (step.querySelector('.quiz__register')) return 'register';

		return false;
	};

	p.setErrorText = function(string){
		this.error.innerHTML = string;
	};

	p.checkButton = function(e){
		e.preventDefault();

		var current = this.steps[this.currentIndex];

		if(e.target.classList.contains('disabled')){
			if (this.getStepType(current) === 'answers') this.setErrorText('Выберите один из вариантов ответов');
			if (this.getStepType(current) === 'date') this.setErrorText('Укажите корректную(-ые) дату(-ы)');
			if (this.getStepType(current) === 'register') this.setErrorText('Укажите верные данные');

			this.show(this.error);
		}

		if(e.target === this.next && !e.target.classList.contains('disabled')) this.timoutNext();

		if(e.target === this.send && !this.send.classList.contains('disabled')){
			this.setInfoToLast();
			this.timoutNext();
			// setTimeout(this.sendData, 5000);
		}
	}; 

	p.enableButtons = function(){
		this.next.classList.remove('disabled');
		this.send.classList.remove('disabled');
	};

	p.disableButtons = function(){
		this.next.classList.add('disabled');
		this.send.classList.add('disabled');
	};

	p.addButtonsListeners = function(){
		this.next.addEventListener('click', this.checkButton);
		this.send.addEventListener('click', this.checkButton);
		this.prev.addEventListener('click', this.prevStep);
	};

	p.addFormListener = function(){
		this.form.addEventListener('submit', function(e){
			e.preventDefault();
		});
	};

	p.checkCompleted = function(){
		if(this.completed[this.currentIndex] === null) this.disableButtons();
		if(this.completed[this.currentIndex] !== null){
			this.enableButtons();
			this.hide(this.error);
		}
	};

	p.removeClassFromAll = function(items, className){
		this.each(items, function(item){
			item.classList.remove('active');
		});
	};

	p.checkCompletedArray = function(completed){
		var trigger = true;

		this.each(completed, function(item){
			if(item === null) trigger = false; 
		});

		return trigger;
	};

	p.validateEmail = function(email) {
		var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
		return re.test(String(email).toLowerCase());
	};

	p.manageAnswers = function(step){
		var labels = step.querySelectorAll('.quiz__radio-label'),
			self = this;

		this.each(labels, function(label, index){
			var input = label.querySelector('.quiz__radio');

			input.addEventListener('change', function(){
				if(input.checked === false) return;

				self.removeClassFromAll(labels, 'active');
				label.classList.add('active');
				// Сохраняем индекс выбранного элемента в массив
				self.completed[self.currentIndex] = index;
				self.hide(self.error);
				self.timoutNext();
				// Запуск вариации при изменении инпута
				self.makeVariation();
			});
		});
	};

	p.checkInput = function(input, type){
		if(type === 'date'){
			// var year = this.today.getFullYear(),
			// 	yearInMilliseconds = 31536000000,
			// 	selectedYear = parseInt(input.value.split('.')[2]),
			// 	selectedMonth = parseInt(input.value.split('.')[1]),
			// 	selectedDay = parseInt(input.value.split('.')[0]),
			// 	date = Date.parse(selectedYear + '-' + selectedMonth + '-' + selectedDay);

			// if (date < (this.today - yearInMilliseconds) && selectedYear > (year - 150) && selectedYear <= year) return 'completed';
			// else return null;
			
			// if(input.selected)
			
			if(input.value !== '') return 'completed';
			else return null;
		}

		if(type === 'text'){
			if(input.value !== '' && input.value.length > 1) return 'completed';
			else return null;
		}

		if(type === 'email'){
			if(this.validateEmail(input.value)) return 'completed';
			else return null;
		}

		return null;
	};

	// Проверяем спрятанные инпуты, если инпут спрятан, то он будет отмечен как завершенный
	p.checkHiddenInputs = function(inputs, array){
		this.each(inputs, function(input, index){
			if(input.classList.contains('_hidden')) array[index] = 'completed';
		});

		return array;
	};

	p.manageInputs = function(step, inputs){
		var completed = [];

		this.each(inputs, function(input){
			completed.push(null);
		});

		var self = this;

		this.each(inputs, function(input, index){
			// Получаем тип инпута для его проверки
			var type = input.getAttribute('data-type'),
				check = function(e){
					// Проверяем спрятанные инпуты, если инпут спрятан, то он будет отмечен как завершенный
					completed = self.checkHiddenInputs(inputs, completed);

					completed[index] = self.checkInput(input, type);

					// Разрешаем доступ к кнопкам "Далее" если все заполнено как надо
					if(self.checkCompletedArray(completed)) self.completed[self.currentIndex] = 'completed';
					else self.completed[self.currentIndex] = null;

					self.checkCompleted();
				};

			input.addEventListener('change', check);
			input.addEventListener('keydown', check);
			input.addEventListener('input', check);
		});
	};

	p.manageStep = function(step){
		if (this.getStepType(step) === 'answers') this.manageAnswers(step);
		if (this.getStepType(step) === 'date') this.manageInputs(step, step.querySelectorAll('.select__real'));
		if (this.getStepType(step) === 'register') this.manageInputs(step, step.querySelectorAll('.quiz-register__input'));
	};

	p.setInfoToLast = function(){
		var last = this.steps[this.steps.length - 1],
			form = new FormData(this.form),
			lastTitle = last.querySelector('.quiz__subtitle_last span'),
			lastMail = last.querySelector('.quiz__mail span');

		lastTitle.innerHTML = form.get('name');
		lastMail.innerHTML = form.get('email');
	};

	$.urlParam = function(name){
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		if (results==null) {
		   return null;
		}
		return decodeURI(results[1]) || 0;
	}

	p.sendData = function(){
		var form = new FormData(this.form),
			string = 'https://ai.atmacode.com/auto_register/',
			womanDate = '',
			manDate = '';

		if(form.get('woman_year') !== null && form.get('woman_month') !== null && form.get('woman_day') !== null){
			womanDate = form.get('woman_year') + '-' + form.get('woman_month') + '-' + form.get('woman_day');
		}

		if(form.get('man_year') !== null && form.get('man_month') !== null && form.get('man_day') !== null){
			manDate = form.get('man_year') + '-' + form.get('man_month') + '-' + form.get('man_day');
		}

		let utm_source = $.urlParam('utm_source');
		let utm_medium = $.urlParam('utm_medium');
		let utm_campaign = $.urlParam('utm_campaign');
		let utm_content = $.urlParam('utm_content');
		let utm_term = $.urlParam('utm_term');

		string += "?firstname=" + form.get('name');
		string += "&email=" + form.get('email');
		string += "&n1=" + form.get('name') + "&n2="+ form.get('name');
		string += "&d1="+ manDate +"&d2="+ womanDate;
		string += "&compat=1";
		string += "&utm_source=" + utm_source;
		string += "&utm_medium=" + utm_medium;
		string += "&utm_campaign=" + utm_campaign;
		string += "&utm_content=" + utm_content;
		string += "&utm_term=" + utm_term;

		window.location.href = string;
	};

	p.checkVariation = function(el){
		var array = el.getAttribute('data-hide').split(';'),
			self = this;

		this.show(el);
		el.classList.remove('_hidden');

		this.each(array, function(item){
			var arr = item.split(',');

			if(self.completed[arr[0]] === parseInt(arr[1])){
				self.hide(el);
				el.classList.add('_hidden');
			}
		});
	};

	p.makeVariation = function(){
		var variations = this.main.querySelectorAll('*[data-hide]');

		this.each(variations, this.checkVariation);
	};

	w.Quiz = Quiz;
})(window);


(function(w){
	var Select = function(element){
		if(!element) return;

		if (typeof element === 'string') {
			element = document.querySelectorAll(element);
		}

		if (element.length > 1) {
			return Select.createMany(element);
		} else if (element.length === 1) {
			element = element[0];
		}

		if(element.length === 0) return;

		this.main = element;
		this.select = this.main.querySelector('.select__real');
		this.options = [];
		this.createSelectReplacement();
		this.createReplacementItems();
		this.bindHandlers();
		this.addListeners();
	};

	Select.createMany = function (nodes) {
		var result = [];
		for (var i = 0; i < nodes.length; i++) {
			result.push(new Select(nodes[i]));
		}
		return result;
	};

	var p = Select.prototype;

	p.cE = function(string){
		return document.createElement(string);
	};


	p.each = function(items, callback){
		[].forEach.call(items, callback);
	};

	p.bindHandlers = function(){
		this.documentMouseupHandler = this.documentMouseupHandler.bind(this);
	};

	/*
	* Добавляем в массив элементы необходимые для проверки на нажатие
	* Принимает массив который нужно дополнить и элементы, которые нужно добавить в массив
	*/
	p.completeTargetArray = function(arr, items){
		var j = 0,
			arrLength = arr.length;
		for (var i = arrLength; i < items.length + arrLength; i++) {
			arr[i] = items[j];
			j++;
		}

		return arr;
	};

	/*
	* Проверка на нажатие допустимых элементов.
	* Принимает массив элементов и событие
	*/
	p.checkTargetElements = function(arr, event){
		if (arr.length === 0) return false;

		var trigger = false;

		for (var i = 0; i < arr.length; i++) {
			if (event.target === arr[i] && !trigger) trigger = true;
		}

		return trigger;
	};

	/**
	 * Создаем оболочку для замены select
	*/
	p.createSelectReplacement = function() {
		this.container = this.cE('div');
		this.current = this.cE('div');
		this.items = this.cE('div');
		this.outer = this.cE('div');
		this.inner = this.cE('div');

		this.container.classList = 'select__container';
		this.current.classList = 'select__current';
		this.items.classList = 'select__items';
		this.outer.classList = 'select__outer';
		this.inner.classList = 'select__inner';

		this.outer.appendChild(this.inner);
		this.items.appendChild(this.outer);

		this.container.appendChild(this.current);
		this.container.appendChild(this.items);

		this.main.appendChild(this.container);
	};

	/**
	 * Добаляем элементы и отмечаем выбранный элемент
	*/
	p.createReplacementItems = function() {
		var items = this.select.options,
			self = this,
			selected = this.select.querySelector('[selected]'),
			disabled = this.select.querySelector('[disabled]');

		this.each(items, function (item, index) {
			var text = item.text,
				option = self.cE('div');

			option.classList = 'select__item';

			if(disabled && disabled === item) option.classList.add('disabled');

			if ((selected && selected === item) || (!selected && index === 0)) {
				option.classList.add('active');
				self.current.innerHTML = text;
			}
			option.innerHTML = text;
			self.inner.appendChild(option);
			self.options.push(option);
		});
	};

	p.changeActive = function(el){
		var active = this.container.querySelector('.select__item.active');
		active.classList.remove('active');
		el.classList.add('active');
		this.current.innerHTML = el.innerHTML;
	};

	p.close = function(){
		this.items.classList.remove('active');
		this.main.classList.remove('active');
	};

	p.open = function(){
		this.items.classList.add('active');
		this.main.classList.add('active');
	};

	p.documentMouseupHandler = function(event){
		var arr = this.completeTargetArray([this.container, this.current, this.items, this.inner, this.main, this.outer], this.options);
		if (!this.checkTargetElements(arr, event)) this.close();
	};

	p.triggerChange = function(){
		var event = new Event('change');
		this.select.dispatchEvent(event);
	};

	// Связываем элементы между собой
	p.addListeners = function(){
		var self = this;

		// Событие при нажатии на один из элементов
		this.each(this.options, function(item, index){
			item.addEventListener('click', function(e){
				if(e.target.classList.contains('disabled')) return false;

				self.changeActive(e.target);
				self.select.options[index].selected = true;

				self.triggerChange();

				self.close();
			});
		});

		// Событие при изменении настоящего select
		this.select.addEventListener('change', function(){
			self.changeActive(self.options[self.select.options.selectedIndex]);
		});

		// Событие при нажатии на созданный селект
		this.current.addEventListener('click', function(){
			if(self.items.classList.contains('active')) self.close();
			else self.open();
		});

		// Ловим событие клика или тапа для закрытия элемента если нажатие не по элементу
		document.body.addEventListener('mouseup', this.documentMouseupHandler);
	};

	w.Select = Select;
})(window);

// main
document.addEventListener("DOMContentLoaded", function() {

	// Генерируем дни с 1 по 31
	function generateSelectDays(select){
		for(var i = 1; i < 32; i++){
			var option = document.createElement('option'),
				value = i;

			if(value < 10) value = '0' + value;

			option.innerHTML = value;
			option.value = value;

			select.appendChild(option);
		}
	} [].forEach.call(document.querySelectorAll('.select__real_day'), generateSelectDays);

	// Генерируем месяца
	function generateSelectMonths(select){
		array = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

		for(var i = 0; i < array.length; i++){
			var option = document.createElement('option'),
				value = i + 1;

			if(value < 10) value = '0' + value;

			option.innerHTML = array[i];
			option.value = value;

			select.appendChild(option);
		}
	} [].forEach.call(document.querySelectorAll('.select__real_month'), generateSelectMonths);

	// Генерируем месяца
	function generateSelectYears(select){
		var currentYear = new Date().getFullYear(),
			firstYear = currentYear - 60;

		for(var i = firstYear; i < currentYear; i++){
			var option = document.createElement('option');

			option.innerHTML = i;
			option.value = i;

			select.appendChild(option);
		}
		
	} [].forEach.call(document.querySelectorAll('.select__real_year'), generateSelectYears);

	// Включаем селекты
	var selects = new Select('.select_dropdown');
	// Включаем квиз
	var quiz = new Quiz('.quiz');
	// Включаем контроль блокировки Body
	var bb = new BodyBlockingControl;
	// Включаем модальное окно
	var mmodal = new Mmodal(bb);
	//lazy load
	var lazyLoadInstance = new LazyLoad({
		elements_selector: ".lazy",
		threshold: 0
	});

	// Допуск только символов в именных инпутах
	function onlyTextInput(e){
		// Возможность писать в текстовом поле только буквы
		if(e.type === 'keypress') if(e.key.match(/[0-9]/)) e.preventDefault();
		if(e.type === 'change' || e.type === 'input') e.target.value = e.target.value.replace(/[0-9]/g, "");
	}

	[].forEach.call(document.querySelectorAll('.only-text'), function(input){
		input.addEventListener('keypress', onlyTextInput);
		input.addEventListener('input', onlyTextInput);
		input.addEventListener('change', onlyTextInput);
	});

	// Событие при изменении размера окна
	function windowResize(callback) {
		window.onresize = function () {
			if (callback) callback();
		}
	};


	function initModalOpening(buttons, modal){
		[].forEach.call(buttons, function(button){
			button.addEventListener('click', function(e){
				e.preventDefault();

				mmodal.open(modal);
			});
		});
	};

	initModalOpening(document.querySelectorAll('a[href="#popup_1"]'), '.modal-quiz');
	initModalOpening(document.querySelectorAll('a[href="#popup_2"]'), '.modal-examples');
	
	// Создаем доты
	function generateDots(embla, dotsContainer, count, className){
		var dots = [];

		for(var i = 0; i < count; i++){
			var dot = document.createElement('li');

			if(embla.selectedScrollSnap() === i) dot.className = className + '__dot carousel__dot';
			else dot.className = className + '__dot carousel__dot';
			
			dotsContainer.appendChild(dot);
			dots.push(dot);
		}

		changeActiveDot(dots, embla);
		return dots;
	}

	// Добавляем события к дотам
	function addDotsEvents(dots, embla){
		for(var i = 0; i < dots.length; i++){
			dots[i].addEventListener('click', function(e){
				e.preventDefault();
				embla.scrollTo(dots.indexOf(this));

				restartAutoplay(embla);
			});
		}
	}

	// Меняем активный дот
	function changeActiveDot(dots, embla){
		var activeIndex = embla.selectedScrollSnap();

		for(var i = 0; i < dots.length; i++){
			dots[i].classList.remove("active");
			dots[i].classList.remove("prev");
			dots[i].classList.remove("prev-prev");
			dots[i].classList.remove("next");
			dots[i].classList.remove("next-next");
		}

		if(activeIndex > 0) dots[activeIndex - 1].classList.add("prev");
		if(activeIndex < dots.length - 1) dots[activeIndex + 1].classList.add("next");

		if(dots.length >= 5){
			if(activeIndex === 2) dots[0].classList.add("prev-prev");
			if(activeIndex > 2) dots[activeIndex - 2].classList.add("prev-prev");
			if(activeIndex < dots.length - 2) dots[activeIndex + 2].classList.add("next-next");
		} 

		dots[embla.selectedScrollSnap()].classList.add("active");
	}

	// Создаем контейнер для дотов
	function createDotsContainer(carousel, className){
		var dotsContainer = document.createElement('ul');
		dotsContainer.className = className + '__dots carousel__dots row';
		carousel.appendChild(dotsContainer);

		return dotsContainer;
	}

	// Запускаем создание дотов
	function initEmblaDots(embla, carousel, className){
		var slidesCount = embla.slideNodes().length,
			dotsContainer = createDotsContainer(carousel, className),
			dots = generateDots(embla, dotsContainer, slidesCount, className);

		addDotsEvents(dots, embla);
		embla.on('scroll', function(){
			if(dots.length > 0) changeActiveDot(dots, embla);
		});
	}

	// Указываем активный слайд
	function selectActiveSlide(embla){
		for(var i = 0; i < embla.slideNodes().length; i++) embla.slideNodes()[i].classList.remove("active");
		embla.slideNodes()[embla.selectedScrollSnap()].classList.add("active");
	}

	// Автоплей слайдера
	function autoplay(embla, interval) {
		var timer = 0;

		var play = function () {
			stop();
			requestAnimationFrame(function () {
				timer = window.setTimeout(next, interval);
				return timer;
			});
		};

		var stop = function () {
			window.clearTimeout(timer);
			timer = 0;
		};

		var next = function () {
			if (embla.canScrollNext()) {
				embla.scrollNext();
			} else {
				embla.scrollTo(0);
			}

			play();
		};

		return { play: play, stop: stop };
	}

	function restartAutoplay(embla){
		if(embla.autoplayer){
			embla.autoplayer.stop();
			embla.autoplayer.play();
		}
	}

	function autoHeight(slides, embla){
		// var slides = embla.slideNodes();
		
		[].forEach.call(slides, function(slide){
			slide.style.height = 'auto';
			slide.style.height = slide.offsetHeight + 'px';
		});

		if(embla){
			embla.reInit();
			restartAutoplay(embla);
		}
	}

	function createCarousel(carousel, options){
		if(carousel.classList.contains('carousel_vertical')){
			options.axis = 'y';
			if (window.innerWidth < 769) options.align = 'start';
			else options.align = 'center';
		}

		var viewport = carousel.querySelector('.carousel__viewport'),
			embla = EmblaCarousel(viewport, options),
			className = carousel.getAttribute('data-classname'),
			prev = carousel.querySelector('.carousel__prev'),
			next = carousel.querySelector('.carousel__next');

		if(carousel.classList.contains('carousel_autoplay')){
			embla.autoplayer = autoplay(embla, 2000);

			embla.on("pointerDown", function(){
				restartAutoplay(embla);
			});
			embla.on("init", embla.autoplayer.play);
		}

		// Автоподстройка height слайдов
		if(carousel.classList.contains('carousel_autoheight')){
			embla.on('resize', function(){ autoHeight(embla.slideNodes(), embla); });

			embla.on('init', function(){
				setTimeout(function(){
					autoHeight(embla.slideNodes(), embla);
				}, 300);
			   
			});
		}

		embla.on('resize', function(){
			if (window.innerWidth < 769 && carousel.classList.contains('carousel_vertical')) options.align = 'start';
			else options.align = 'center';

			embla.reInit(options);
		});
			
		// Добавление дотов к слайдеру
		if(carousel.classList.contains('carousel_dots')) initEmblaDots(embla, carousel, className);

		embla.on('scroll', function(){selectActiveSlide(embla)});
		embla.on('init', function(){selectActiveSlide(embla)});

		// Назад
		if (prev) {
			prev.addEventListener('click', function () {
				embla.scrollPrev();

				restartAutoplay(embla);
			});
		}

		// Вперед
		if (next) {
			next.addEventListener('click', function () {
				embla.scrollNext();

				restartAutoplay(embla);
			});
		}
	}

	function getCarouselOptions(){
		var options = {
				speed: 20,
				skipSnaps: false,
				// containScroll: 'keepSnaps',
				draggable: true,
				loop: true,
				align: 'center'
			};

		// Разрешаем драггинг только в мобильных устройствах
		if((document.body.offsetWidth + bb.bar) > 991) options.draggable = false;
		else options.draggable = true;

		return options;
	}

	[].forEach.call(document.querySelectorAll('.carousel'), function(carousel){
		createCarousel(carousel, getCarouselOptions());
	});

	// scroll to elem
	function scrollTo() {
		var button = document.querySelectorAll('.scroll-to');

		for(var i = 0; i < button.length; i++){
			button[i].addEventListener('click', function(e){
				e.preventDefault();

				var to = this.getAttribute('href');
				scrollToElement(to, {
					offset: 0,
					ease: 'in-out-expo',
					duration: 600
				});
			});
		}
	} scrollTo();
	// end scroll to elem
 
	function burgerMenu() {
	  var burger = document.querySelector(".burger-container"),
		  header = document.querySelector(".header__burger"),
		  links = header.querySelectorAll('a');

	  burger.addEventListener('click', function(){

		if(!header.classList.contains("menu-opened")){
			header.classList.add("menu-opened");

			bb.block();
		}else{
			header.classList.remove("menu-opened");

			bb.unblock();
		}
	  });

	  [].forEach.call(links, function(link){
		link.addEventListener('click', function(e){
			if(this.classList.contains('scroll-to')){
				e.preventDefault();
				 header.classList.remove("menu-opened");
				bb.unblock();
			}
		})
	  });
	} burgerMenu();

	function playVideo() {
	  //Воспроизведение видео
	  var play = document.querySelectorAll(".card__review");
	  play.forEach(function (btnPlay) {
		btnPlay.addEventListener("click", function (event) {
		  var activeVideo = btnPlay.querySelector(".card__video");
		  var allVideos = document.querySelectorAll(".card__video");
		  var activeInfo = btnPlay.querySelector(".card__info");
		  var allCardInfo = document.querySelectorAll(".card__info");

		  if (activeVideo.paused) {
			allVideos.forEach(function (videoItem) {
			  videoItem.pause();
			});
			activeVideo.play();
			allCardInfo.forEach(function (infoItem) {
			  infoItem.classList.remove("hidden");
			});
			activeInfo.classList.add("hidden");
			play.forEach(function (card) {
			  card.classList.remove("hidden");
			});
			btnPlay.classList.add("hidden");
		  } else {
			activeVideo.pause();
			activeInfo.classList.remove("hidden");
			btnPlay.classList.remove("hidden");
		  }
		});
	  });
	} playVideo();


});